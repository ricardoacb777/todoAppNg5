requirements: ## Installing local requirements
ifeq ($(os), Darwin)
	grep -q 'brew' <<< echo `command -v brew` || /usr/bin/ruby -e "`curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install`"
	brew install make git
	brew cask install docker
	open -a Docker
else ifeq ($(os), Linux)
	sudo sh -c "$$(curl -fsSL https://get.docker.com)"
	sudo apt-get install make git
	sudo usermod -aG docker $${USER}
	sudo service docker start
	sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(os)-$(shell uname -m) -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	su - $${USER}
else ifeq ($(os), Windows_NT)
	@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
	choco install make git docker-for-windows -y
else
	$(warning Platform "$(os)" not supported)
endif

build: ## 📦  Build the ap in Docker containers
	docker-compose up --build -d --remove-orphans $(container)
	@echo "Watching to port 4200..."

up:
	docker-compose up -d

reload: up ## Reload environment

down:
	docker-compose down

stop:
	docker-compose stop

start:
	docker-compose start

restart:
	docker-compose restart app

reboot: down up

ps: ## Status of the environment
	docker-compose ps

status: ps

cli exec: container ?= app
cli exec: ## Execute commands in containers, use "command"  argument to send the command. By Default enter the shell.
	docker-compose exec $(container) ash $(command)

run: container ?= app
run: ## 👟  Run commands in a new container
	docker-compose run --rm $(container) ash $(command)


config:
	docker-compose config

logs: ## See logs of the containers
	docker-compose logs -f app

.DEFAULT_GOAL := install

install: build 