import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../TaskModel';
import { getNsPrefix } from '@angular/compiler';

@Component({
  selector: 'task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {
  @Input() task: Task;
  @Output() changeTask: EventEmitter<Task>;
  @Output() deleteTask: EventEmitter<Task>;
  editing: boolean;
  constructor() {
    this.changeTask = new EventEmitter<Task>();
    this.deleteTask = new EventEmitter<Task>();
    this.editing = false;
  }

  onChange(): void {
    if (this.task.title == '') return;
    this.changeTask.emit(this.task);
    this.editing = false;
  }
  onDeleteTask(): void {
    this.deleteTask.emit(this.task);
  }

  editTask (i: HTMLInputElement) {
    this.editing = true;
    console.log(i);
  }

  ngOnInit() {
  }

}
