import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Task } from './TaskModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TodoAppNg5';
  tasks: Task[];
  newTaskForm: FormGroup;
  keyWordPersistedTodos = 'Todos-Angular5';
  constructor (fb: FormBuilder) {
    // this.tasks = [
    //   new Task('Tarea 1', false),
    //   new Task('Tarea 2', true),
    //   new Task('Tarea 3', false)
    // ];
    let persistedTodos = JSON.parse(localStorage.getItem(this.keyWordPersistedTodos) || '[]');
    this.tasks = persistedTodos.map((task: Task) => {
      return new Task(task.title, task.completed);
    });
    this.newTaskForm = fb.group({
      'titleTask': '',
      'toggleCheck': false
    });
  }
  public onChangeTask(task: Task): void {
    const i: number = this.tasks.indexOf(task);
    this.tasks[i].completed = task.completed;
    this.updateStore();
    if (this.calcStatesAll(task.completed)) {
      this.newTaskForm.controls['toggleCheck'].setValue(task.completed);
    } else {
      this.newTaskForm.controls['toggleCheck'].setValue(false);
    }
    console.log(this.tasks);
  }
  public onDeleteTask(task: Task): void {
    this.tasks.splice(this.tasks.indexOf(task), 1);
    if (this.tasks.length == 0) this.newTaskForm.controls['toggleCheck'].setValue(false);
    this.updateStore();
  }
  public onSubmit() {
    this.addTask();
  }
  public addTask() {
    const title = this.newTaskForm.controls['titleTask'].value;
    if (title.trim() == '') return;
    this.tasks.push(new Task(title, false));
    this.newTaskForm.controls['titleTask'].setValue('');
    this.newTaskForm.controls['toggleCheck'].setValue(false);
    this.updateStore();
  }
  public checkAll (checkBox: HTMLInputElement) {
    if (this.tasks.length == 0) {
      this.newTaskForm.controls['toggleCheck'].setValue(false);
      return ;
    }
    this.tasks.forEach(t => {
      t.completed = this.newTaskForm.controls['toggleCheck'].value;
    });
    this.updateStore();
  }
  public calcStatesAll (state: boolean): boolean {
    let countState = 0;
    this.tasks.forEach(e => {
      if (e.completed == state) countState += 1;
    });
    if (countState == this.tasks.length) {
      return true;
    }
    return false;
  }
  private updateStore() {
    localStorage.setItem(this.keyWordPersistedTodos, JSON.stringify(this.tasks));
  }
}
